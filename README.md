Systemd sockets to suspend and halt on connection.

Install those files in /usr/local/lib/systemd/system/

Enable it with
```
# systemctl enable systemd-suspend.socket
# systemctl enable systemd-halt.socket
```

Then the system will suspend on any incoming connection on port 2111
and halt on port 2112 (End of world !).
